﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System.IO;
using System.Xml;

namespace _4_ConsoleApp1
{
    interface ISerializer<T>
    {
        string Serialize<T>(T item);
        T Deserialize<T>(Stream stream);
    }

    class OtusXmlSerializer<T> : ISerializer<T>
    {
        private static readonly IExtendedXmlSerializer serializer;

        static OtusXmlSerializer()
        {
            serializer = new ConfigurationContainer()
             .EnableImplicitTyping(typeof(T))
             .UseOptimizedNamespaces()
             .Create();
        }

        public T Deserialize<T>(Stream stream)
        {
             return serializer.Deserialize<T>(new XmlReaderSettings { IgnoreWhitespace = false }, stream);
        }

        public string Serialize<T>(T item)
        {
            return serializer.Serialize(new XmlWriterSettings { Indent = true }, item);
        }
    }
}