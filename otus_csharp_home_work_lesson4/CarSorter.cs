﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _4_ConsoleApp1
{
    interface ISorter<T>
    {
        IEnumerable<T> Sort();
    }
    class CarSorter : ISorter<Car>
    {
        OtusStreamReader<Car> streamReader;

        public CarSorter(OtusStreamReader<Car> streamReader)
        {
            this.streamReader = streamReader;
        }
        public IEnumerable<Car> Sort()
        {
            return streamReader.OrderBy(a => a.Model);
        }
    }
}
