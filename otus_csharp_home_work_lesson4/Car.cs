﻿using System.Collections.Generic;

namespace _4_ConsoleApp1
{
    public class Car
    {
        public string StockNumber { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }

        public override string ToString()
        {
            return $"Model={Model}\n" +
                   $"Make={Make}\n" +
                   $"StockNumber={StockNumber}\n";
        }
    }
}
