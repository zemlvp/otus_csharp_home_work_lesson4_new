﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace _4_ConsoleApp1
{
    class Program
    {
        static void Main()
        {
            string carsXmlFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "cars.xml");

            var serializer = new OtusXmlSerializer<Car>();

            #region test serialization
            var c = new List<Car>() {
                new Car { Model = "Лада", Make = "ВАЗ", StockNumber = "1000" },
                new Car { Model = "Мерс", Make = "вава", StockNumber = "1000000" },
            };

            var xml = serializer.Serialize<List<Car>>(c);
            #endregion;

            // Задание 1
            Task1(carsXmlFile, serializer);

            // Задание 2
            Task2(carsXmlFile, serializer);    

            // Задание 3
            Task3();

            Console.WriteLine();
            Console.WriteLine("Нажмите любую клавишу");
            Console.ReadKey();
        }

        private static void Task3()
        {
            string jsonFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "accounts.json");

            var account1 = new Account
            {
                FirstName = "Александр",
                LastName = "Пушкин",
                BirthDate = new DateTime(1799, 6, 6)
            };

            var account2 = new Account
            {
                FirstName = "Михаил",
                LastName = "Лермонтов",
                BirthDate = new DateTime(1814, 10, 11)
            };

            var account3 = new Account
            {
                FirstName = "Николай",
                LastName = "Носов",
                BirthDate = new DateTime(1908, 11, 23)
            };

            var testAccounts = new List<Account> { account1, account2, account3 };
            var bank = new Bank { Accounts = testAccounts };
            File.WriteAllText(jsonFile, JsonConvert.SerializeObject(bank));

            var repository = new AccountJsonRepository(jsonFile);
            var accountService = new AccountService(repository);

            accountService.AddAccount(new Account
            {
                FirstName = "Николай",
                LastName = "Гоголь",
                BirthDate = new DateTime(1809, 4, 1)
            });

            var account4 = repository.GetOne(a => a.FirstName == "Александр");
            var accounts = repository.GetAll();
        }

        private static void Task2(string carsXmlFile, OtusXmlSerializer<Car> serializer)
        {
            Console.WriteLine("После сортировки");
            Console.WriteLine();

            using (var stream = new FileStream(carsXmlFile, FileMode.Open))
            using (var streamReaderCars = new OtusStreamReader<Car>(stream, serializer))
            {
                var carSorter = new CarSorter(streamReaderCars);
                var cars = carSorter.Sort();

                foreach (var car in cars)
                {
                    Console.WriteLine(car);
                }
            }
        }

        private static void Task1(string carsXmlFile, OtusXmlSerializer<Car> serializer)
        {
            using (var stream = new FileStream(carsXmlFile, FileMode.Open))
            using (var streamReaderCars = new OtusStreamReader<Car>(stream, serializer))
            {
                foreach (var car in streamReaderCars)
                {
                    Console.WriteLine(car);
                }
            }
            Console.WriteLine(new string('-', 80));
        }
    }
}