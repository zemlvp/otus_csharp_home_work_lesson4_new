﻿using System;

namespace _4_ConsoleApp1
{
    public interface IAccountService
    {
        // В классе-реализаторе делать валидацию: проверить что имена не пустые, что возраст > 18 лет, можете также добавить свои правила
        // Если валидация прошла успешно, то добавлять аккаунт в репозиторий
        void AddAccount(Account account);
    }

    public class AccountService : IAccountService
    {
        IRepository<Account> repository;
        public AccountService(IRepository<Account> repository)
        {
            this.repository = repository;
        }

        public void AddAccount(Account account)
        {
            if (!IsValid(account))
                throw new InvalidOperationException();

            repository.Add(account);
        }

        private int GetAge(DateTime birthdate)
        {
            var today = DateTime.Today;

            var age = today.Year - birthdate.Year;

            // если родился в високосном году
            if (birthdate.Date > today.AddYears(-age))
                age--;

            return age;
        }

        private bool IsValid(Account account)
        {
            bool isValid = false;

            if (!string.IsNullOrEmpty(account.FirstName) &&
                !string.IsNullOrEmpty(account.LastName) &&
                GetAge(account.BirthDate) > 18)
                isValid = true;

            return isValid;
        }
    }
}
