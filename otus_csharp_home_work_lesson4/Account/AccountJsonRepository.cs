﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace _4_ConsoleApp1
{
    // В классе реализаторе сохранять данные в какой-нибудь файл. Формат на ваше усмотрение - json, xml, csv, etc
    public interface IRepository<T>
    {
        // когда реализуете этот метод, используйте yield (его можно использовать просто в методе, без создания отдельного класса)
        IEnumerable<T> GetAll();
        T GetOne(Func<T, bool> predicate);
        void Add(T item);
    }

    public class AccountJsonRepository : IRepository<Account>
    {
        private string jsonFile;
        private Bank bank;

        public AccountJsonRepository(string jsonFile)
        {
            this.jsonFile = jsonFile;
            bank = JsonConvert.DeserializeObject<Bank>(File.ReadAllText(jsonFile));
        }

        public void Add(Account item)
        {
            bank.Accounts.Add(item);
            File.WriteAllText(jsonFile, JsonConvert.SerializeObject(bank));
        }

        public IEnumerable<Account> GetAll()
        {
            foreach (var account in bank.Accounts)
            {
                yield return account;
            }
        }

        public Account GetOne(Func<Account, bool> predicate)
        {
            return bank.Accounts.Where(predicate).SingleOrDefault();
        }
    }
}