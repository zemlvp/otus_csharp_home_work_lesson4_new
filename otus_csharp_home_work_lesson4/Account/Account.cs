﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4_ConsoleApp1
{
    public class Account
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
    }
}
