﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace _4_ConsoleApp1
{
    class OtusStreamReader<T> : IEnumerable<T>, IDisposable
    {
        // Flag: Has Dispose already been called?
        private bool disposed = false;

        private readonly Stream stream;
        private readonly ISerializer<T> serializer;

        public OtusStreamReader(Stream stream, ISerializer<T> serializer)
        {
            this.stream = stream;
            this.serializer = serializer;
        }

        public IEnumerator<T> GetEnumerator()
        {
            List<T> t = serializer.Deserialize<List<T>>(stream);

            for (int i = 0; i <= t.Count - 1; i++)
                yield return t[i];
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
        public void Dispose()
        {
            Console.WriteLine("Сработал Dispose...");
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern 
        // https://docs.microsoft.com/ru-ru/dotnet/standard/garbage-collection/implementing-dispose
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any managed objects here.
                //
            }

            disposed = true;
        }

        ~OtusStreamReader()
        {
            Dispose(false);
        }
    }
}
