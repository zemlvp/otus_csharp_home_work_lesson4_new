using _4_ConsoleApp1;
using Moq;
using NUnit.Framework;
using System;

namespace NUnitTestProject1
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void TestAddAccount()
        {
            var account4 = new Account
            {
                FirstName = "Barak",
                LastName = "Obama",
                BirthDate = new DateTime(1921, 12, 10)
            };

            var mock = new Mock<IRepository<Account>>();
            mock.Setup(a => a.Add(account4));

            var accountService = new AccountService(mock.Object);
            accountService.AddAccount(account4);

            mock.Verify(a => a.Add(account4));
        }
    }
}